<?php

declare(strict_types=1);

namespace App\Tests\Unit;

use App\Entity\Item;
use App\Entity\Todolist;
use App\Entity\User;
use App\Service\EmailService;
use App\Service\ToDoListService;
use DateTime;
use PHPUnit\Framework\TestCase;

class ToDoListServiceTest extends TestCase
{
    /**
     * Test the Todolist service until it send email.
     * 
     * The Todolist service needs the email service as argument, thus the
     * email service must be mocked.
     * Then, the todolist service goes add as enought items as necessary so
     * the Todolist service could send an email (a fake email, due of a mocked email service).
     */
    public function testCanAddItemAndSendEmail(): void
    {
        // Initialisation
        $faker = \Faker\Factory::create('fr_FR');
        $adminEmail = $faker->email();
        $email = $faker->email();
        $user = new User();
        $item = new Item();
        $todolist = new Todolist();

        // Set some properties
        $item = $item->setCreatedAt(new DateTime('now'));
        $user = $user->setEmail($email)
            ->setTodolist($todolist);

        // Create Email service for the Todolist service and create its comportment
        $emailService = $this->createMock(EmailService::class);
        $emailService->expects($this->once())
            ->method('simpleSend')
            ->withConsecutive([
                $adminEmail,
                $email,
                'Your todolist is almost full',
                'As described in subject, your todolist have many items, 8 exactly, and it remains 2 places for adding items.'
            ]);

        // Create todolist service
        $service = new ToDoListService($emailService, $adminEmail);

        // Assert the todolist add one item
        $service->add($user, $item);
        $this->assertEquals(1, $user->getTodolist()->getItems()->count(), 'Item not added');

        // Add 8 items (until the email service will be called)
        for ($i = 1; $i < 8; $i++) {
            $item = new Item();
            $item = $item
                ->setName('element' . $i)
                ->setCreatedAt((new DateTime('now'))->add(new \DateInterval('PT' . (30 * $i) . 'M')));
            $service->add($user, $item);
        }
    }
}
