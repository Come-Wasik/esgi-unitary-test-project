<?php

declare(strict_types=1);

namespace App\Tests\Unit;

use App\Entity\Todolist;
use App\Entity\User;
use DateTime;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
    /**
     * Test initial values of the entity
     */
    public function testInitialValues(): void
    {
        $user = new User();
        $this->assertNull($user->getFirstname());
        $this->assertNull($user->getLastname());
        $this->assertNull($user->getEmail());
        $this->assertNull($user->getPassword());
        $this->assertNull($user->getBirthday());
        $this->assertNull($user->getTodolist());
    }

    /**
     * Test getter and setters + other (e.g. add, remove)
     */
    public function testAffectation(): void
    {
        // Variable declarations
        $faker = \Faker\Factory::create('fr_FR');
        $user = new User();
        $fakeData = [
            'firstname' => $faker->firstName,
            'lastname' => $faker->lastName,
            'email' => $faker->email,
            'password' => 'ruelabrouettedu36',
            'birthday' => (new DateTime('now'))->sub(new \DateInterval('P13Y')),
            'todolist' => new Todolist()
        ];

        // Automatized affectation
        foreach ($fakeData as $key => $value) {
            $setter = 'set' . ucfirst($key);
            $user = $user->$setter($value);
        }

        // Automatized retrivation
        foreach ($fakeData as $key => $value) {
            $getter = 'get' . ucfirst($key);
            $this->assertEquals($value, $user->$getter(), "The original $key property has change its value with the setter");
        }
    }

    /**
     * Check if there is an exception thrown when not goal the minimum size for a password
     */
    public function testCheckWithPasswordMinCount()
    {
        // Variable declarations
        $faker = \Faker\Factory::create('fr_FR');
        $user = new User();
        $user = $user->setEmail($faker->email)
            ->setPassword('1234567')
            ->setBirthday((new DateTime('now'))->sub(new \DateInterval('P13Y')));

        // Exception exceptation
        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage('There is/are error(s): the password must have a size between 8 and 40 characters');
        $this->expectExceptionCode(500);

        // Run the method who must throw an exception
        $user->checkValidity();
    }

    /**
     * Check if there is an exception thrown when passed the maximum size for a password
     */
    public function testCheckWithPasswordMaxCount()
    {
        // Variable declarations
        $faker = \Faker\Factory::create('fr_FR');
        $user = new User();
        $user = $user->setEmail($faker->email)
            ->setPassword($faker->word(41))
            ->setBirthday((new DateTime('now'))->sub(new \DateInterval('P13Y')));

        // Exception exceptation
        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage('There is/are error(s): the password must have a size between 8 and 40 characters');
        $this->expectExceptionCode(500);

        // Run the method who must throw an exception
        $user->checkValidity();
    }

    /**
     * Check if there is an exception thrown when the email is incorrect
     */
    public function testCheckWithEmailValidity()
    {
        // Variable declarations
        $faker = \Faker\Factory::create('fr_FR');
        $notAnEmail = $faker->name;
        $user = new User();
        $user = $user->setEmail($notAnEmail)
            ->setPassword($faker->word(20))
            ->setBirthday((new DateTime('now'))->sub(new \DateInterval('P13Y')));

        // Exception exceptation
        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage('There is/are error(s): the email ' . $notAnEmail . ' is incorrect');
        $this->expectExceptionCode(500);

        // Run the method who must throw an exception
        $user->checkValidity();
    }

    /**
     * Check if there is an exception thrown when the user does not has the minimum age
     */
    public function testCheckWithAgeValidity()
    {
        // Variable declarations
        $faker = \Faker\Factory::create('fr_FR');
        $user = new User();
        $user = $user->setEmail($faker->email)
            ->setPassword($faker->word(20))
            ->setBirthday((new DateTime('now'))->sub(new \DateInterval('P12Y')));

        // Exception exceptation
        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage('the age of the user must be more than 13 years old');
        $this->expectExceptionCode(500);

        // Run the method who must throw an exception
        $user->checkValidity();
    }
}
