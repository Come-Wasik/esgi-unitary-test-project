<?php

declare(strict_types=1);

namespace App\Tests\Unit;

use App\Service\EmailService;
use Monolog\Test\TestCase;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

class EmailServiceTest extends TestCase
{
    /**
     * Test the use of the Send method
     */
    public function testCanTryToSendEmail()
    {
        // Variable initialisation
        $faker = \Faker\Factory::create('fr_FR');
        $fromEmail = $faker->email;
        $toEmail = $faker->email;
        $subject = 'The subject';
        $text = 'The text';

        // Create mailer
        $mailer = $this->createMock(MailerInterface::class);

        // Create email
        $email = (new Email())
            ->from($fromEmail)
            ->to($toEmail)
            ->subject($subject)
            ->text($text);

        // Affect email to mailer
        $mailer
            ->expects($this->once())
            ->method('send')
            ->with($this->equalTo($email));

        // Create email service
        $service = new EmailService($mailer);
        $service->simpleSend($fromEmail, $toEmail, $subject, $text);
    }
}
