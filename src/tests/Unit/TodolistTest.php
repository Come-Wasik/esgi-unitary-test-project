<?php

declare(strict_types=1);

namespace App\Tests\Unit;

use App\Entity\Item;
use App\Entity\Todolist;
use App\Entity\User;
use DateTime;
use PHPUnit\Framework\TestCase;

class TodolistTest extends TestCase
{
    /**
     * Test initial values of the entity.
     */
    public function testInitialValues(): void
    {
        $todolist = new Todolist();
        $this->assertNull($todolist->getUtilisateur(), 'The todolist user property MUST be null by default');
        $this->assertEquals(0, $todolist->getItems()->count(), 'The item list MUST be empty by default');
    }

    /**
     * Test getter and setters + other (e.g. add, remove).
     */
    public function testAffectation(): void
    {
        $user = new User();
        $item = new Item();
        $todolist = new Todolist();

        $todolist->setUtilisateur($user);
        $item = $item->setCreatedAt(new \DateTime('now'));
        $todolist->addItem($item);

        $this->assertEquals(1, $todolist->getItems()->count(), 'Item not added');
        $this->assertEquals($user, $todolist->getUtilisateur(), 'User not added');
        $this->assertContains($item, $todolist->getItems(), 'Item not added');

        $todolist->removeItem($item);
        $this->assertEquals(0, $todolist->getItems()->count(), 'Item not removed');
    }

    /**
     * Test that no item can be added without a createdAt property.
     */
    public function testItemRequireCreatedAtToBeAdded(): void
    {
        $item = new Item();
        $todolist = new Todolist();

        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage('You want to add an item which has no createdAt property');
        $this->expectExceptionCode(500);

        $todolist->addItem($item);
    }

    /**
     * Test that name in each item is unique.
     */
    public function testIfEachNameAreUnique(): void
    {
        $todolist = new Todolist();
        $item = new Item();

        $item = $item->setCreatedAt(new \DateTime('now'));
        $todolist->addItem($item);

        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage('You cannot add the item because it already exists');
        $this->expectExceptionCode(500);

        $todolist->addItem($item);
    }

    /**
     * Test if the todolist can have maximum 10 items
     */
    public function testMaximalCountOfElement(): void
    {
        $todolist = new Todolist();
        for ($i = 1; $i < 11; $i++) {
            $item = new Item();
            $item = $item->setName('element' . $i)
                // Create a date with a delay of 30 minutes each time
                ->setCreatedAt((new DateTime('now'))->add(new \DateInterval('PT' . (30 * $i) . 'M')));
            $todolist->addItem($item);
        }

        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage('You cannot add the item because the maximum quantity is 10');
        $this->expectExceptionCode(500);

        $item = new Item();
        $item = $item->setName('element11');
        $todolist->addItem($item);
    }

    /**
     * Test that between 2 items added, there is a delay of 30 minutes.
     * 
     * The function will create 3 items:
     * + Item A and Item B have a delay bigger than 30 minutes
     * + Item B and Item C have a delay lower than 30 minutes
     */
    public function testDelayBetweenItems(): void
    {
        $todolist = new Todolist();
        $itemA = new Item();
        $itemB = new Item();
        $itemC = new Item();

        $itemA = $itemA->setCreatedAt(new DateTime('now'));
        $itemB = $itemB->setCreatedAt((new DateTime('now'))->add(new \DateInterval('PT30M')));
        $itemC = $itemC->setCreatedAt((new DateTime('now'))->add(new \DateInterval('PT35M')));

        $todolist->addItem($itemA);
        $todolist->addItem($itemB);

        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessageRegExp('#^You cannot add the item because the delay with the last item.#');
        $this->expectExceptionCode(500);

        $todolist->addItem($itemC);
    }
}
