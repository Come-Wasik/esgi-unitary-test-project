# [ESGI - 4IW2]

![PHP](https://img.shields.io/badge/PHP-%5E7.4-blue) ![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline/Come-Wasik/esgi-unitary-test-project/master)

## A LIRE
La méthode `isValid()` de l'entité User a été changé en `checkValidity()` car il est plus pertinant de ne pas retourner un booléan valant vrai si au même endroit est mit en place un système d'exceptions. Il vaut mieux alors, n'utiliser que ce système d'exception.

La méthode `checkValidity()` renvoi une exception si une ou plusieurs erreurs sont trouvés, rien en d'autres cas.

## Description

This is a symfony project, about unitary test. This is for a school project at ESGI.
The web project is in the src directory. Then, as all symfony 5 projects:
+ Classes are in the directory `/src/Entity`
+ Tests are in the directory `/tests`

## Requirement

PHP 7.4

## Usage

```bash
php bin/phpunit --configuration phpunit.xml.dist
```